﻿using System;

using UnityEngine;

using UnityEditor;

using System.Collections;

using System.Collections.Generic;



[CustomEditor(typeof(CameraLibrary))]

public class CameraLibraryInspector : Editor

{


    public override void OnInspectorGUI()

    {

        CameraLibrary lib = target as CameraLibrary;

        



        EditorGUILayout.BeginHorizontal();

        GUILayout.Label("Key");

        GUILayout.Label("Prefab");

        bool bAddNew = GUILayout.Button("New");

        EditorGUILayout.EndHorizontal();



        if (bAddNew)

        {

            lib.Set("New Key #" + lib.Keys.Count, null);

        }



        for (int nIndex = 0; nIndex < lib.Keys.Count; ++nIndex)

        {

            string key = lib.Keys[nIndex];

            Camera obj = lib.Values[nIndex];



            EditorGUILayout.BeginHorizontal();

            lib.Keys[nIndex] = GUILayout.TextField(key);

            lib.Values[nIndex] = EditorGUILayout.ObjectField(obj, typeof(Camera), true) as Camera;

            bool bDelete = GUILayout.Button("Delete");

            EditorGUILayout.EndHorizontal();



            if(bDelete)

            {

                lib.Remove(key);

                break;

            }

        }



        EditorUtility.SetDirty(target);

    }

}

