﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using Retro.Command;
using System;

using Hashtable = ExitGames.Client.Photon.Hashtable;
using SupportClassPun = ExitGames.Client.Photon.SupportClass;

public class PhotonManager : Photon.PunBehaviour, IPunObservable // MonoBehaviour, IPunObservable, IPunCallbacks
{
	public delegate void PhotonEvent();
    public delegate void PhotonObjectEvent(object[] objectData);
    public delegate void PhotonPlayerEvent(PhotonPlayer newMasterClient);
    public delegate void PhotonDisconnectEvent(DisconnectCause cause);
	public delegate void PhotonDataEvent(byte eventcode, object content, int senderid);

	public delegate void PhotonObservableEvent(PhotonStream stream, PhotonMessageInfo info);
	public delegate void PhotonInstantiateEvent(PhotonMessageInfo info);
    public delegate void PhotonCustomRoomPropertiesChangedEvent(Hashtable propertiesThatChanged);
    public delegate void CustomAuthenticationFailedEvent(string debugMessage);
    public delegate void CustomAuthenticationResponseEvent(Dictionary<string, object> data);
    public delegate void WebRpcResponseEvent(OperationResponse response);

    public PhotonEvent OnConnectedToPhotonEvent;
    public PhotonEvent OnLeftRoomEvent;
    public PhotonEvent OnLobbyStatisticsUpdateEvent;
    public PhotonEvent OnCreatedRoomEvent;
    public PhotonEvent OnJoinedLobbyEvent;
    public PhotonEvent OnLeftLobbyEvent;
    public PhotonEvent OnJoinedRoomEvent;
    public PhotonEvent OnConnectedToMasterEvent;
    public PhotonEvent OnPhotonMaxCccuReachedEvent;
    public PhotonEvent OnUpdatedFriendListEvent;
    public PhotonEvent OnDisconnectedFromPhotonEvent;
    public PhotonEvent OnReceivedRoomListUpdateEvent;
	public PhotonDataEvent OnPhotonDataEvent;

    public PhotonObjectEvent OnPhotonCreateRoomFailedEvent;
    public PhotonObjectEvent OnPhotonJoinRoomFailedEvent;
    public PhotonObjectEvent OnPhotonRandomJoinFailedEvent;
    public PhotonObjectEvent OnOwnershipRequestEvent;
	public PhotonObjectEvent OnOwnershipTransferedEvent;
    public PhotonObjectEvent OnPhotonPlayerPropertiesChangedEvent;

    public PhotonPlayerEvent OnMasterClientSwitchedEvent;
    public PhotonPlayerEvent OnPhotonPlayerConnectedEvent;
    public PhotonPlayerEvent OnPhotonPlayerDisconnectedEvent;
	public PhotonPlayerEvent OnPhotonPlayerActivityChangedEvent;

    public PhotonDisconnectEvent OnFailedToConnectToPhotonEvent;
    public PhotonDisconnectEvent OnConnectionFailEvent;

    public PhotonObservableEvent OnPhotonSerializeViewEvent;	
    
	public PhotonInstantiateEvent OnPhotonInstantiateEvent;
    public PhotonCustomRoomPropertiesChangedEvent OnPhotonCustomRoomPropertiesChangedEvent;
    public CustomAuthenticationFailedEvent OnCustomAuthenticationFailedEvent;
    public CustomAuthenticationResponseEvent OnCustomAuthenticationResponseEvent;
    public WebRpcResponseEvent OnWebRpcResponseEvent;

	[SerializeField]
	private string _gameVersion = "1";

	[SerializeField]
	private byte _playersPerRoom = 2; // probably dont need to serialize

	[SerializeField]
	private PhotonLogLevel _LogLevel = PhotonLogLevel.Full;

	private RoomOptions _RoomOptions;

	void Awake()
	{
		PhotonNetwork.autoJoinLobby = false;
		PhotonNetwork.automaticallySyncScene = false;

		PhotonNetwork.logLevel = _LogLevel;
		 _RoomOptions = new RoomOptions() { MaxPlayers = (byte)_playersPerRoom };
	}

	void OnEnable()
	{
    	PhotonNetwork.OnEventCall += OnDataEvent;
	}

	void OnDisable()
	{
    	PhotonNetwork.OnEventCall -= OnDataEvent;
	}

	void OnDataEvent(byte eventcode, object content, int senderid)
	{
		if(OnPhotonDataEvent != null)
		{
			OnPhotonDataEvent.Invoke(eventcode, content, senderid);
		}
	}

	public void RaiseDataEvent(byte eventCode, object content, bool relighable = true, RaiseEventOptions options = null)
	{
		PhotonNetwork.RaiseEvent(eventCode, content, relighable, options);
	}

	[RetroCommand]
	public bool Connect()
	{ 
		if (!PhotonNetwork.connected)
		{
			return PhotonNetwork.ConnectUsingSettings(_gameVersion);
		}
		return true;
	}

	[RetroCommand]
	public bool JoinLobby()
	{
		if (PhotonNetwork.connected && !PhotonNetwork.insideLobby)
		{
			return PhotonNetwork.JoinLobby();
		}
		return false;
	}

	[RetroCommand]
	public bool LeaveLobby()
	{
		if (PhotonNetwork.connected && PhotonNetwork.insideLobby)
		{
			return PhotonNetwork.LeaveLobby();
		}
		return false;
	}

	public RoomInfo[] GetAvailableRooms()
	{
		RoomInfo[] rooms = PhotonNetwork.GetRoomList();
		List<RoomInfo> roomList = new List<RoomInfo>();
		foreach(RoomInfo room in rooms)
		{
			if(room.MaxPlayers > room.PlayerCount)
			{
				roomList.Add(room);
			}
		}
		return roomList.ToArray();
	}

	[RetroCommand]
	public bool JoinRoom()
	{
		RoomInfo[] aviableRooms = GetAvailableRooms();
		RoomInfo connectedRoom = aviableRooms.Length > 0 ? aviableRooms[0] : null;
		if (connectedRoom != null)
		{
			return PhotonNetwork.JoinOrCreateRoom(connectedRoom.Name, _RoomOptions, TypedLobby.Default);
		}
		else
		{
			return PhotonNetwork.JoinOrCreateRoom("Room " + Guid.NewGuid().ToString(), _RoomOptions, TypedLobby.Default);
		}
	}

	[RetroCommand]
	public bool CreateRoom()
	{
		// NOTE: apparently CreateRoom with these args is a server only call....  if issues arise this is why
		return PhotonNetwork.CreateRoom(null, _RoomOptions, TypedLobby.Default);
	}

	[RetroCommand]
	public bool LeaveRoom()
	{
		return PhotonNetwork.LeaveRoom(false);
	}

	public int NumPlayersInRoom()
	{
		if(PhotonNetwork.inRoom && PhotonNetwork.room != null)
		{
			return PhotonNetwork.room.PlayerCount;// == PhotonNetwork.room.MaxPlayers;
		}
		return -1;
	}

	public bool CanStartGame()
	{
		if(PhotonNetwork.inRoom && PhotonNetwork.room != null)
		{
			return PhotonNetwork.room.PlayerCount == PhotonNetwork.room.MaxPlayers;
		}
		return false;
	}

	public bool IsRoomOwner()
	{
		if(PhotonNetwork.inRoom && PhotonNetwork.room != null)
		{
			return PhotonNetwork.isMasterClient;
		}
		return false;
	}

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if(OnPhotonSerializeViewEvent != null)
		{
			OnPhotonSerializeViewEvent.Invoke(stream, info);
		}
	}
	
    public override void OnConnectedToPhoton()
	{
		if(OnConnectedToPhotonEvent != null)
		{
			OnConnectedToPhotonEvent.Invoke();
		}
	}

    public override void OnLeftRoom()
	{
		if(OnLeftRoomEvent != null)
		{
			OnLeftRoomEvent.Invoke();
		}
	}

    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
	{
		if(OnMasterClientSwitchedEvent != null)
		{
			OnMasterClientSwitchedEvent.Invoke(newMasterClient);
		}
	}

    public override void OnPhotonCreateRoomFailed(object[] codeAndMsg)
	{
		if(OnPhotonCreateRoomFailedEvent != null)
		{
			OnPhotonCreateRoomFailedEvent.Invoke(codeAndMsg);
		}
	}

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
	{
		if(OnPhotonJoinRoomFailedEvent != null)
		{
			OnPhotonJoinRoomFailedEvent.Invoke(codeAndMsg);
		}
	}

    public override void OnCreatedRoom()
	{
		if(OnCreatedRoomEvent != null)
		{
			OnCreatedRoomEvent.Invoke();
		}
	}

    public override void OnJoinedLobby()
	{
		if(OnJoinedLobbyEvent != null)
		{
			OnJoinedLobbyEvent.Invoke();
		}
	}

    public override void OnLeftLobby()
	{
		if(OnLeftLobbyEvent != null)
		{
			OnLeftLobbyEvent.Invoke();
		}
	}

    public override void OnFailedToConnectToPhoton(DisconnectCause cause)
	{
		if(OnFailedToConnectToPhotonEvent != null)
		{
			OnFailedToConnectToPhotonEvent.Invoke(cause);
		}
	}

    public override void OnConnectionFail(DisconnectCause cause)
	{
		if(OnConnectionFailEvent != null)
		{
			OnConnectionFailEvent.Invoke(cause);
		}
	}

    public override void OnDisconnectedFromPhoton()
	{
		if(OnDisconnectedFromPhotonEvent != null)
		{
			OnDisconnectedFromPhotonEvent.Invoke();
		}
	}

    public override void OnPhotonInstantiate(PhotonMessageInfo info)
	{
		if(OnPhotonInstantiateEvent != null)
		{
			OnPhotonInstantiateEvent.Invoke(info);
		}
	}

    public override void OnReceivedRoomListUpdate()
	{
		if(OnReceivedRoomListUpdateEvent != null)
		{
			OnReceivedRoomListUpdateEvent.Invoke();
		}
	}

    public override void OnJoinedRoom()
	{
		if(OnJoinedRoomEvent != null)
		{
			OnJoinedRoomEvent.Invoke();
		}
	}

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
	{
		if(OnPhotonPlayerConnectedEvent != null)
		{
			OnPhotonPlayerConnectedEvent.Invoke(newPlayer);
		}
	}

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
	{
		if(OnPhotonPlayerDisconnectedEvent != null)
		{
			OnPhotonPlayerDisconnectedEvent.Invoke(otherPlayer);
		}
	}

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
	{
		if(OnPhotonRandomJoinFailedEvent != null)
		{
			OnPhotonRandomJoinFailedEvent.Invoke(codeAndMsg);
		}
	}

    public override void OnConnectedToMaster()
	{
		if(OnConnectedToMasterEvent != null)
		{
			OnConnectedToMasterEvent.Invoke();
		}
	}

    public override void OnPhotonMaxCccuReached()
	{
		if(OnPhotonMaxCccuReachedEvent != null)
		{
			OnPhotonMaxCccuReachedEvent.Invoke();
		}
	}

    public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
	{
		if(OnPhotonCustomRoomPropertiesChangedEvent != null)
		{
			OnPhotonCustomRoomPropertiesChangedEvent.Invoke(propertiesThatChanged);
		}
	}

    public override void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
	{
		if(OnPhotonPlayerPropertiesChangedEvent != null)
		{
			OnPhotonPlayerPropertiesChangedEvent.Invoke(playerAndUpdatedProps);
		}
	}

    public override void OnUpdatedFriendList()
	{
		if(OnUpdatedFriendListEvent != null)
		{
			OnUpdatedFriendListEvent.Invoke();
		}
	}

    public override void OnCustomAuthenticationFailed(string debugMessage)
	{
		if(OnCustomAuthenticationFailedEvent != null)
		{
			OnCustomAuthenticationFailedEvent.Invoke(debugMessage);
		}
	}

    public override void OnCustomAuthenticationResponse(Dictionary<string, object> data)
	{
		if(OnCustomAuthenticationResponseEvent != null)
		{
			OnCustomAuthenticationResponseEvent.Invoke(data);
		}
	}

    public override void OnWebRpcResponse(OperationResponse response)
	{
		if(OnWebRpcResponseEvent != null)
		{
			OnWebRpcResponseEvent.Invoke(response);
		}
	}

    public override void OnOwnershipRequest(object[] viewAndPlayer)
	{
		if(OnOwnershipRequestEvent != null)
		{
			OnOwnershipRequestEvent.Invoke(viewAndPlayer);
		}
	}

    public override void OnLobbyStatisticsUpdate()
	{
		if(OnLobbyStatisticsUpdateEvent != null)
		{
			OnLobbyStatisticsUpdateEvent.Invoke();
		}
	}

	public override void OnPhotonPlayerActivityChanged(PhotonPlayer otherPlayer)
	{
		if(OnPhotonPlayerActivityChangedEvent != null)
		{
			OnPhotonPlayerActivityChangedEvent.Invoke(otherPlayer);
		}
	}

	public override void OnOwnershipTransfered(object[] viewAndPlayers)
	{
		if(OnOwnershipTransferedEvent != null)
		{
			OnOwnershipTransferedEvent.Invoke(viewAndPlayers);
		}
	}
}
