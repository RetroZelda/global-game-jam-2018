﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(CanvasGroup))]
public class Pulse : MonoBehaviour 
{
	[System.Serializable]
	private enum Direction {ToMin, ToMax};
	
	private CanvasGroup _Canvas;

	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float _MinAlpha = 0.0f;

	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float _MaxAlpha = 1.0f;

	[SerializeField]
	private float _Speed = 1.0f;

	[SerializeField]
	private Direction _StartAlpha = Direction.ToMax;

	private float _CurAlpha;
	private Direction _CurDirection;

	void Awake () 
	{
		_Canvas = GetComponent<CanvasGroup>();

		switch(_StartAlpha)
		{
			case Direction.ToMax:
				_CurAlpha = _MaxAlpha;
				_CurDirection = Direction.ToMin;
			break;
			case Direction.ToMin:
				_CurAlpha = _MinAlpha;
				_CurDirection = Direction.ToMax;
			break;
		}
	}

	void OnDisable()
	{
		_Canvas.alpha = 0.0f;
	}
	
	void Update () 
	{
		switch(_CurDirection)
		{
			case Direction.ToMax:
				_CurAlpha += _Speed * Time.deltaTime;
				if(_CurAlpha >= _MaxAlpha)
				{
					_CurDirection = Direction.ToMin;
					_CurAlpha = _MaxAlpha;
				}
			break;
			case Direction.ToMin:
				_CurAlpha -= _Speed * Time.deltaTime;
				if(_CurAlpha <= _MinAlpha)
				{
					_CurDirection = Direction.ToMax;
					_CurAlpha = _MinAlpha;
				}
			break;
		}
		_Canvas.alpha = _CurAlpha;
	}
}
