﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioListener))]
public class AudioManager : MonoBehaviour 
{
	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float _BGMVolume = 0.5f;

	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float _SFXVolume = 0.5f;

	private AudioListener _Listener;
	private AudioSource _BGM;
	private AudioSource _SFX;

	void Start () 
	{
		_Listener = GetComponent<AudioListener>();
		_BGM = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
		_SFX = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;

		SetBGMVolume(_BGMVolume);
		SetSFXVolume(_SFXVolume);
	}

#if UNITY_EDITOR
	void Update()
	{
		SetBGMVolume(_BGMVolume);
		SetSFXVolume(_SFXVolume);
	}
#endif

	public void SetBGMPitch(float fPitch)
	{
		_BGM.pitch = fPitch;
	}

	public void SetBGMVolume(float fVolume)
	{
		_BGM.volume = fVolume;
	}

	public void SetSFXVolume(float fVolume)
	{
		_SFX.volume = fVolume;
	}
	
	public void PlayBGM(AudioClip clip, float fPitch = 1.0f, bool bForce = false)
	{
		if(clip != _BGM.clip || bForce)
		{
			_BGM.clip = clip;
			_BGM.loop = true;
			_BGM.Play();

			SetBGMVolume(_BGMVolume);
		}
	}

	public void PlaySFX(AudioClip clip, float fPitch = 1.0f)
	{
		_SFX.clip = clip;
		_SFX.loop = false;
		_SFX.pitch = fPitch;
		_SFX.Play();

		SetSFXVolume(_SFXVolume);
	}
}
