﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour 
{
    [SerializeField]
    private Camera _Camera;

    void Start()
    {
        if(!_Camera)
        {
            _Camera = FindObjectOfType<TopDownCameraFollower>().GetComponentInChildren<Camera>();
        }
    }

    void LateUpdate()
    {
        Vector3 dir = transform.position - _Camera.transform.position;
        // dir.y = 0.0f;
        transform.rotation = Quaternion.LookRotation(dir);

        // transform.LookAt(_Camera.transform.position, -Vector3.up);
    }
}
