﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionAttach : MonoBehaviour 
{
	public Transform Root {get; set;}
	
	void LateUpdate () 
	{
		if(Root)
		{
			transform.position = Root.position;
		}
	}
}
