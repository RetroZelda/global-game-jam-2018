﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraLibrary : MonoBehaviour

{
    [SerializeField]
    private List<string> _keys = new List<string>();

    [SerializeField]
    private List<Camera> _Values = new List<Camera>();

    

    public List<string> Keys { get { return _keys; } set { _keys = value; } }
    public List<Camera> Values { get { return _Values; } set { _Values = value; } }

    private Camera _ActiveCamera = null;

    void Update()
    {
        if(_ActiveCamera)
        {
            Global.MainCamera.transform.position = Vector3.Lerp(Global.MainCamera.transform.position, _ActiveCamera.transform.position, 0.5f);
            Global.MainCamera.transform.rotation = Quaternion.Lerp(Global.MainCamera.transform.rotation, _ActiveCamera.transform.rotation, 0.5f);
        }
    }

    public void Set(string szKey, Camera value)

    {

        int nIndex = _keys.FindIndex((k) => k == szKey);

        if(nIndex < 0)

        {

            _keys.Add(szKey);

            _Values.Add(value);

        }

        else

        {

            _Values[nIndex] = value;

        }

    }

    public void Remove(string szKey)

    {

        int nIndex = _keys.FindIndex((k) => k == szKey);

        if (nIndex >= 0)

        {

            _keys.RemoveAt(nIndex);

            _Values.RemoveAt(nIndex);

        }

    }

    public Camera Obtain(string szKey)

    {

        int nIndex = _keys.FindIndex((k) => k == szKey);

        if (nIndex >= 0)

        {

            return _Values[nIndex];

        }

        return null;

    }

    public void SetActiveCamera(string szKey)
    {
        _ActiveCamera = Obtain(szKey);
    }
}
