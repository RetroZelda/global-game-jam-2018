﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// because sometimes the canvas wont update unless its position is attempted to move
public class CanvasHack : MonoBehaviour 
{
	void Update () 
	{
		transform.Translate(0.0f, 1.0f, 0.0f);	
	}
}
