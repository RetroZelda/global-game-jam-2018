﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpinner : MonoBehaviour 
{
	[SerializeField]
	private Vector3 _Axis = Vector3.up;

	[SerializeField]
	private float _Speed = 1.0f;

	private float _fCurAngle;

	void Start()
	{
		_fCurAngle = 0.0f;
	}

	void Update () 
	{
		transform.Rotate(_Axis, _Speed * Time.deltaTime);
	}
}
