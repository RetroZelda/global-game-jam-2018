﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenRunner : MonoBehaviour 
{
    public delegate void RunnerCompleteEvent();

    [SerializeField]
    private Camera _Cam;

    [SerializeField]
    private Transform _Runner;

    [SerializeField]
    private float _Speed = 1.0f;

    private bool _bIsRunning = false;

    public RunnerCompleteEvent OnRunnerComplete;


	// Use this for initialization
	void Start ()
    {
        Reset();
	}

    void Update()
    {
        if (_bIsRunning)
        {
            _Runner.Translate(Vector3.forward * _Speed * Time.deltaTime);
            Vector3 v3CP = _Cam.WorldToViewportPoint(_Runner.localPosition);
            if (v3CP.x > 1.5f)
            {
                if (OnRunnerComplete != null)
                {
                    OnRunnerComplete.Invoke();
                }
                Reset();
            }
        }
    }
	
    public void Reset()
    {
        Vector3 v3Pos = _Cam.ViewportToWorldPoint(new Vector3(-0.50f, 0.0f, 0.0f));
        _Runner.localPosition = new Vector3(v3Pos.x, _Runner.localPosition.y, _Runner.localPosition.z);

        _bIsRunning = false;
    }

    public void StartRunner()
    {
        _bIsRunning = true;
    }
}
