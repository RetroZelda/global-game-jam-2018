﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using Retro.Input;
using Retro.Command;
using DG.Tweening;

[RequireComponent(typeof(LineRenderer))] // NOTE: this is base type; probably sphere for the player but cant be sure...
public class LineDrawer : MonoBehaviour 
{
	public enum LineDrawState { LDS_Idle, LDS_Drawing }
	public delegate void OnPointAddedEvent(Vector3 point);
	public delegate void OnPathDrawnEvent(Vector3[] Path);

	public OnPointAddedEvent OnPointAdded;
	public OnPathDrawnEvent OnPathDrawn;

	[SerializeField]
	private float _MaxDistance = 10.0f;

	[SerializeField]
	private Camera _GameCamera; // needed to determine if we touched ourself

	// private Collider _Collider;
	private LineRenderer _Liner; // I dont remember how to use this

	public LineDrawState CurDrawState { get; protected set; }
	public Camera GameCamera 
	{
		 get { return _GameCamera ?? Global.MainCamera; } 
		 set { _GameCamera = value; } // in case we want the game controller to set the camera(managing multiple cameras or something; dont be stupid with this)
	}

	private Retro.Log.RetroLogger _Logger;
	protected Retro.Log.RetroLogger Logger
	{
		get { return _Logger ?? Retro.Log.RetroLog.RetrieveCatagory("LineDrawer"); }
	}

	private List<Vector3> _LineData;
	private float _CurDistance;
	private Vector3 _v3StartPointCache;
	private bool _bCanDraw = false;

	void Start()
	{
		_LineData = new List<Vector3>();
		// _Collider = GetComponent<Collider>();
		_Liner = GetComponent<LineRenderer>();
		CurDrawState = LineDrawState.LDS_Idle;
		_CurDistance = 0.0f;
	}

	void OnEnable()
	{
		LeanTouch.OnFingerDown 	+= OnTouchDown;
		LeanTouch.OnFingerUp	+= OnTouchUp;
		LeanTouch.OnFingerSet	+= OnTouchDrag;
	}

	void OnDisable()
	{
		LeanTouch.OnFingerDown 	-= OnTouchDown;
		LeanTouch.OnFingerUp	-= OnTouchUp;
		LeanTouch.OnFingerSet	-= OnTouchDrag;
	}

	[RetroCommand]
	public void StartDrawing()
	{
		_bCanDraw = true;
	}

	[RetroCommand]
	public void FinishDrawing()
	{
		_bCanDraw = false;
		if(OnPathDrawn != null)
		{
			OnPathDrawn.Invoke(_LineData.ToArray());
		}
	}
	
	private void OnTouchDown(LeanFinger finger)
	{
		if(!_bCanDraw)
		{
			return;
		}

		if(GameCamera == null)
		{
			Logger.LogWarning("Note: GameCamera not set!");
		}

		// Ray ray = finger.GetRay(GameCamera);
		// RaycastHit hitInfo;
		// if(DoesHitSelf(ray, out hitInfo))
		{
			// Logger.Log(string.Format("Hit self: {0}", hitInfo.transform.position));
			// if(transform == hitInfo.transform) // probably should assert instead?
			{
				_v3StartPointCache = transform.position;
				if(_LineData.Count == 0)
				{
					AddPoint(_v3StartPointCache); 
					_Liner.positionCount++;
				}
				_Liner.SetPosition(_Liner.positionCount - 1, _v3StartPointCache); // last point is live pos
				CurDrawState = LineDrawState.LDS_Drawing;
			}
		}
	}

	private void OnTouchUp(LeanFinger finger)
	{
		if(!_bCanDraw)
		{
			return;
		}
		if(CurDrawState == LineDrawState.LDS_Drawing)
		{
			Ray ray = finger.GetRay(GameCamera);
			RaycastHit hitInfo;
			if(DoesHitCanvas(ray, out hitInfo))
			{
				Vector3 v3HitPoint = hitInfo.point;
				float deltaDist = Vector3.Distance(_v3StartPointCache, v3HitPoint);
				// _CurDistance += deltaDist;
				// if(_CurDistance > _MaxDistance)
				// {
					// clamp the new point to the max dist
					// _CurDistance = _MaxDistance;
					// v3NewPoint = LimitPointFilter(_v3StartPointCache, v3NewPoint, deltaDist);
				// }

				transform.forward = (v3HitPoint - _v3StartPointCache).normalized;
				transform.position = _v3StartPointCache;
				transform.DOMove(v3HitPoint, 0.5f);
				AddPoint(v3HitPoint);
			}
		}
		CurDrawState = LineDrawState.LDS_Idle;
	}

	private void OnTouchDrag(LeanFinger finger)
	{
		if(!_bCanDraw)
		{
			return;
		}
		if(CurDrawState == LineDrawState.LDS_Drawing)
		{
			// does leantouch delta scale properly? probably not
			Ray ray = finger.GetRay(GameCamera);
			RaycastHit hitInfo;
			if(DoesHitCanvas(ray, out hitInfo))
			{
				Vector3 v3HitPoint = hitInfo.point;
				v3HitPoint.y = transform.position.y; // lock to Y

				// float deltaDist = Vector3.Distance(_v3StartPointCache, v3HitPoint);
				// float fTempDist = _CurDistance + deltaDist;
				// if(fTempDist > _MaxDistance)
				// {
				// 	// clamp the new point to the max dist
				// 	deltaDist = fTempDist - _MaxDistance;
				// 	v3HitPoint = LimitPointFilter(_v3StartPointCache, v3HitPoint, deltaDist);
				// }
								
				_Liner.SetPosition(_Liner.positionCount - 1, v3HitPoint);
			}
		}
	}

	private bool DoesHitSelf(Ray ray, out RaycastHit hitInfo)
	{
		float fToSelf = Vector3.Distance(transform.position, ray.origin); // lol sqrt
		return Physics.Raycast(ray, out hitInfo, fToSelf * 2.0f); // TODO: Probably layer for performance?
	}

	private bool DoesHitCanvas(Ray ray, out RaycastHit hitInfo)
	{
		float fToSelf = Vector3.Distance(transform.position, ray.origin); // lol sqrt
		return Physics.Raycast(ray, out hitInfo, fToSelf * 2.0f); // TODO: Probably layer for performance?
	}

	[RetroCommand]
	private void ClearPoints()
	{
		if(_LineData == null)
		{
			_LineData = new List<Vector3>();
		}
		_Liner.positionCount = 0;
		_CurDistance = 0.0f;
	}

	[RetroCommand]
	private void AddPoint(Vector3 point, bool IsAbsolute = true)
	{
		_LineData.Add(point + new Vector3(0.0f, 0.5f, 0.0f));
		_Liner.SetPosition(++_Liner.positionCount - 1, point);
		if(OnPointAdded != null)
		{
			OnPointAdded.Invoke(point);
		}
	}

	[RetroCommand]
	private void RemoveLastPoint()
	{
		_Liner.positionCount--;
	}

	private Vector3 LimitPointFilter(Vector3 start, Vector3 end, float Dist)
	{
		Vector3 dir = (end - start).normalized;
		return start + (dir * Dist);
	}
}
