﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Retro.Command;

public class LineTraveler : MonoBehaviour 
{
	public enum LineTravelState { LTS_Idle, LTS_Ready, LTS_Traveling }
	public delegate void PathEvent(Transform trans);

	public PathEvent OnPathEnd;

	[SerializeField]
	private float _TravelSpeed;

	private LineTravelState _CurTravelState;
	private Vector3[] _Path;
	private Vector3 _SimulatedPosition;
	private Vector3 _NodeStartPosition;
	private int _CurPathIndex;

	public bool IsDoneWithPath
	{
		get { return _CurPathIndex >= _Path.Length; }
	}

	public bool IsTraveling
	{
		get { return _CurTravelState == LineTravelState.LTS_Traveling; }
	}

	// NOTE: Verify with IsDoneWithPath
	public Vector3 CurPathPoint
	{
		get { return _Path[_CurPathIndex]; }
	}

	public float CurSegmentMagnitude
	{
		get
		{
			return Vector3.Distance(CurPathPoint, _NodeStartPosition);
		}
	}

	void Start()
	{
	}

	void Update()
	{
		if(_CurTravelState == LineTravelState.LTS_Traveling)
		{
			if(!IsDoneWithPath)
			{
				Vector3 v3FinalPos = CurPathPoint;
				Vector3 v3NewPos = Vector3.MoveTowards(_SimulatedPosition, v3FinalPos, _TravelSpeed * Global.FixedStep);
				float fDist = Vector3.Distance(v3NewPos, v3FinalPos);
				Vector3 v3Delta = v3NewPos - _SimulatedPosition;
				transform.forward = v3Delta.normalized;
				transform.transform.position += v3Delta;
				_SimulatedPosition = v3NewPos;
				if(fDist < float.Epsilon)
				{
					_SimulatedPosition = v3FinalPos;
					_CurPathIndex++;
					if(IsDoneWithPath)
					{
						StopTraveling();

						if(OnPathEnd != null)
						{
							OnPathEnd.Invoke(transform);
						}
					}
				}
			}
		}
	}

	void OnDrawGizmos()
	{
		if(_CurTravelState == LineTravelState.LTS_Traveling)
		{
			Gizmos.DrawSphere(_SimulatedPosition, 0.05f);
		}
	}

	[RetroCommand]
	public void StartTraveling()
	{
		if(_CurTravelState == LineTravelState.LTS_Ready)
		{
			_CurTravelState = LineTravelState.LTS_Traveling;
			_CurPathIndex = 0;
			transform.position = CurPathPoint;
			_NodeStartPosition = CurPathPoint;
			_CurPathIndex = 1;
		}
	}

	[RetroCommand]
	public void StopTraveling()
	{
		if(_CurTravelState == LineTravelState.LTS_Traveling)
		{
			_CurTravelState = LineTravelState.LTS_Ready;
		}
	}

	public void SetPath(Vector3[] path)
	{
		if(path.Length > 0)
		{
			_CurTravelState = LineTravelState.LTS_Ready;
			_Path = path;
			_CurPathIndex = 0;
			_SimulatedPosition = _Path[0];
		}
	}
}
