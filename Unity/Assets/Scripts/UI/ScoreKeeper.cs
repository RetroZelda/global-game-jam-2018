﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		Reset();
	}
	
	public void Reset()
	{
		foreach(Transform child in transform)
		{
			child.Find("On").gameObject.SetActive(false);
			child.Find("Off").gameObject.SetActive(true);
		}
	}

	public void SetScoreIndex(int nIndex, bool bOn)
	{
		if(nIndex < transform.childCount)
		{
			Transform child = transform.GetChild(nIndex);
			child.Find("On").gameObject.SetActive(bOn == true);
			child.Find("Off").gameObject.SetActive(bOn == false);
		}
	}
}
