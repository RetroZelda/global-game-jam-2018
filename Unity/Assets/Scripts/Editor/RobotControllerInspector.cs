﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RobotController))]
public class RobotControllerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        RobotController controller = (RobotController)target;
		RobotController.IsCheering = GUILayout.Toggle(RobotController.IsCheering, "Is Cheering");
		
        DrawDefaultInspector();
    }
}