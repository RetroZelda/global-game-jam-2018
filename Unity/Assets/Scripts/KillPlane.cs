﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlane : MonoBehaviour 
{
	public delegate void PlayerDeathEvent(BasePlayer player);
	public PlayerDeathEvent OnPlayerDeath;

	[SerializeField]
	private ParticleSystem _ExplosionOnKill;

	void OnCollisionEnter(Collision col)
	{
		BasePlayer player = col.gameObject.GetComponent<BasePlayer>();
		if(player && OnPlayerDeath != null)
		{
			if(_ExplosionOnKill)
			{
				ParticleSystem particle = Instantiate(_ExplosionOnKill.gameObject).GetComponent<ParticleSystem>();
				particle.transform.position = col.transform.position;
				particle.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
				particle.Play();
			}
			OnPlayerDeath.Invoke(player);
		}
	}
}
