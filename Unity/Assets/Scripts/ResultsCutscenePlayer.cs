﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultsCutscenePlayer : MonoBehaviour 
{
	public delegate void CutsceneFinishedEvent();

	public CutsceneFinishedEvent OnCutsceneFinished;

	[SerializeField]
	private Animator _CarAnimation;

	[SerializeField]
	public Transform WinnerCamera;

	[SerializeField]
	public Transform LoserCamera;
	
	[SerializeField]
	private float _WinAnimationTime = 10.0f;

	[SerializeField]
	private float _LoseAnimationTime = 10.0f;

	private bool IsRunning = false;
	private float _Time;

	public void StartCutscene(bool bWinning)
	{
		IsRunning = true;
		_Time = bWinning == true ? _WinAnimationTime : _LoseAnimationTime;
		_CarAnimation.SetBool("IsWinning", bWinning);
		_CarAnimation.SetBool("IsLosing", !bWinning);
	}

	public void StopCutscene()
	{
		_CarAnimation.SetBool("IsWinning", false);
		_CarAnimation.SetBool("IsLosing", false);
	}

	void Update()
	{
		if(IsRunning)
		{
			_Time -= Time.deltaTime;
			if(_Time < 0.0f)
			{
				IsRunning = false;
				StopCutscene();
				if(OnCutsceneFinished != null)
				{
					OnCutsceneFinished.Invoke();
				}
			}
		}
	}
}
