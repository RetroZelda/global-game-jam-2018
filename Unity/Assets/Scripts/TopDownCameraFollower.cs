﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TopDownCameraFollower : MonoBehaviour 
{
	[SerializeField]
	private Transform _Target;

	[SerializeField]
	private Vector3 _Offset = new Vector3(0.0f, 0.0f, 10.0f);

	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float _Smooth = 0.5f;

	void Start () 
	{
		transform.position = GetTargetPosition();
		transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
	}
	
	void Update () 
	{
		transform.position = Vector3.Lerp(transform.position, GetTargetPosition(), _Smooth);
	}

	private Vector3 GetTargetPosition()
	{
		if(_Target != null)
		{
			return _Target.transform.position + _Offset;
		}
		return transform.position;
	}
}
