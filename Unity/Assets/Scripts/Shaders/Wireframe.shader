﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "FX/Wireframe" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,0,0,1)
	}
	SubShader 
	{
	
	    Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		
			ZWrite Off
			Blend One Zero
			ZTest Greater

    	Pass 
    	{
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			
			uniform float4 _Color;
		
			struct v2g 
			{
    			float4  pos : SV_POSITION;
			};
			
			struct g2f 
			{
    			float4  pos : SV_POSITION;
			};

			struct appdata 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			v2g vert(appdata v)
			{
    			v2g OUT;
    			OUT.pos = UnityObjectToClipPos(v.vertex);
    			return OUT;
			}
			
			[maxvertexcount(3)]
			void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream)
			{
			
				//frag position
				float2 p0 = IN[0].pos.xy / IN[0].pos.w;
				float2 p1 = IN[1].pos.xy / IN[1].pos.w;
				float2 p2 = IN[2].pos.xy / IN[2].pos.w;
				
				//barycentric position
				float2 v0 = p2-p1;
				float2 v1 = p2-p0;
				float2 v2 = p1-p0;
				//triangles area
				float area = abs(v1.x*v2.y - v1.y * v2.x);
			
				g2f OUT;
				OUT.pos = IN[0].pos;
				triStream.Append(OUT);

				OUT.pos = IN[1].pos;
				triStream.Append(OUT);

				OUT.pos = IN[2].pos;
				triStream.Append(OUT);
				
			}
			
			half4 frag(g2f IN) : COLOR
			{ 				
 				return _Color;				
			}
			
			ENDCG

    	}
	}
    FallBack "Diffuse", 1
}