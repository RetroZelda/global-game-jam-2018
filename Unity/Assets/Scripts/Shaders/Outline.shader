Shader "FX/Outline" 
{
	Properties 
	{
		_Color ("Outline Color", Color) = (.5,.5,.5)
		_Outline ("Outline width", Range (.002, 0.03)) = .005
		_Alpha ("Alpha", Range (0.0, 1.0)) = 1.0
	}
	
	CGINCLUDE
	#include "UnityCG.cginc"
	
	ENDCG

	SubShader 
	{
		Tags { "RenderType"="Transparent" }

		Pass 
		{			
			Name "OUTLINE"
			Tags { "LightMode" = "Always" }
			Cull Front
			ZWrite On
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			struct appdata 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				UNITY_FOG_COORDS(0)
				fixed4 color : COLOR;
			};
			
			uniform float _Alpha;
			uniform float _Outline;
			uniform float4 _Color;
			
			v2f vert(appdata v) 
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);

				float3 norm   = normalize(UnityObjectToViewPos(v.normal));
				float2 offset = TransformViewToProjection(norm.xy);
				
				o.pos.xy += offset * o.pos.z * _Outline;
				o.color = _Color;
				o.color.a = _Alpha;
				return o;
			}
			
			fixed4 frag(v2f i) : SV_Target
			{
				return i.color;
			}
			ENDCG
		}
	}
	
	Fallback "Toon/Basic"
}
