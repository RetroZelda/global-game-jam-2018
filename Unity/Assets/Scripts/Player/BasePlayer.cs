﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePlayer : MonoBehaviour 
{
	[Header("Player Settings")]
	[SerializeField]
	private GameObject _RedObject;

	[SerializeField]
	private GameObject _BlueObject;

	private BasePhotonState.PlayerSide _MySide;

	protected GameObject ActiveObject
	{
		get 
		{
			return _MySide == BasePhotonState.PlayerSide.Red ? _RedObject : _BlueObject; 
		}
	}

	public void SetPlayerSide(BasePhotonState.PlayerSide side)
	{
		_MySide = side;

		if(_RedObject)_RedObject.SetActive(side == BasePhotonState.PlayerSide.Red);
		if(_BlueObject)_BlueObject.SetActive(side == BasePhotonState.PlayerSide.Blue);
	}

	// function to enable the object when the gameobject is alive
	public virtual void Activate() {}

	// function to disable the object but keep the gameobject alive
	public virtual void Deactivate() {}
}
