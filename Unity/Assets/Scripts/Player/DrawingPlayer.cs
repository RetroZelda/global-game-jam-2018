﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineDrawer))]
public class DrawingPlayer : BasePlayer
{
	private LineDrawer _Drawer;
	public LineDrawer Drawer
	{
		get { return _Drawer ?? GetComponent<LineDrawer>(); }
	}

	public LineDrawer.OnPathDrawnEvent OnPathDrawn
	{
		get { return Drawer.OnPathDrawn;  }
		set { Drawer.OnPathDrawn = value; }
	}
	
	public LineDrawer.OnPointAddedEvent OnPointAdded
	{
		get { return Drawer.OnPointAdded;  }
		set { Drawer.OnPointAdded = value; }
	}
	
	public void StartDrawing()
	{
		Drawer.StartDrawing();
	}

	public void FinishDrawing()
	{
		Drawer.FinishDrawing();
	}

	// function to enable the object when the gameobject is alive
	public override void Activate() 
	{
		StartDrawing();
	}

	// function to disable the object but keep the gameobject alive
	public override void Deactivate() 
	{
		FinishDrawing();
	}
}
