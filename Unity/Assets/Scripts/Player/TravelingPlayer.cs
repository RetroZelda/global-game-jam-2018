﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineTraveler))]
public class TravelingPlayer : BasePlayer
{
	[SerializeField]
	private ParticleSystem _HitParticles;

	private LineTraveler _Traveler;
	public LineTraveler Traveler
	{
		get { return _Traveler ?? GetComponent<LineTraveler>(); }
	}

	public Rigidbody PhysX { get { return GetComponent<Rigidbody>(); }}

	public LineTraveler.PathEvent OnPathEnd
	{
		get { return Traveler.OnPathEnd; }
		set { Traveler.OnPathEnd = value; }
	}

	public void SetPath(Vector3[] path)
	{
		Traveler.SetPath(path);
	}
	
	public void StartTraveling()
	{
		Traveler.StartTraveling();
	}
	
	public void StopTraveling()
	{
		Traveler.StopTraveling();
	}

	void OnCollisionEnter(Collision other)
	{
		TravelingPlayer player = other.gameObject.GetComponent<TravelingPlayer>();
		if(player != null && player != this)
		{
			// we hit a player
			if(player.Traveler.IsTraveling)
			{
				Vector3 dir = other.contacts[0].point - transform.position;
				dir = dir.normalized;

				_HitParticles.transform.position = other.contacts[0].point;
				_HitParticles.transform.forward = dir;
				_HitParticles.gameObject.SetActive(true);
				_HitParticles.Play();

				// dir = Vector3.Cross(dir, Vector3.up);
				// dir = Vector3.Cross(Vector3.up, dir);
				StartCoroutine(BumpRoutine(-dir, player));
			}
		}
	}

	private IEnumerator BumpRoutine(Vector3 v3Dir, TravelingPlayer player)
	{
		// over N frames, we will translate ourselves from the pump
		float fStrength = player.Traveler.CurSegmentMagnitude * Global.FixedStep;
		for(int nCount = 0; nCount < 10; ++nCount)
		{
			transform.position += (v3Dir * fStrength);
			yield return null;
		}
	}

	// function to enable the object when the gameobject is alive
	public override void Activate() 
	{
		PhysX.isKinematic = false;
	}

	// function to disable the object but keep the gameobject alive
	public override void Deactivate() 
	{
		PhysX.isKinematic = true;
	}
}
