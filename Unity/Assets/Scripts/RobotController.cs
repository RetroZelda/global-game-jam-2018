﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotController : MonoBehaviour 
{
	[Header("Controls")]
	[SerializeField]
	private static bool _IsCheering = false;
	public static bool IsCheering { get { return _IsCheering;}  set { _IsCheering = value;} }

	// [SerializeField]
	// [Range(0.0f, 1.0f)]
	private float _CheerWeight = 0.0f;

	[Header("Settings")]
	[SerializeField]
	private Animator _Animator;

	// Use this for initialization
	void Start () 
	{
		_CheerWeight = Random.Range(0.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_Animator)
		{
			_Animator.SetBool("IsCheering", _IsCheering);
			_Animator.SetFloat("Blend", _CheerWeight);
		}
	}
}
