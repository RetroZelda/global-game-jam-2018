﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Retro.FSM;
using Retro.Log;

[FSMState("PhotonState", "Lobby")]
public class PhotonLobbyState : BasePhotonState
{
	private RectTransform _Lobby;
	private Button _FindGameButton;
	private Button _ExitLobbyButton;
	private bool _bSearchingForRoom;
	private bool _bWaitingForPlayer;

	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);

		_Lobby = UI.Find("Lobby") as RectTransform;
		_FindGameButton = _Lobby.Find("Buttons/FindGame").GetComponent<Button>();
		_ExitLobbyButton = _Lobby.Find("Buttons/ExitLobby").GetComponent<Button>();

		_Lobby.gameObject.SetActive(false);
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);
		
		Photon.OnCreatedRoomEvent += HandleCreateRoom;
		Photon.OnJoinedRoomEvent += HandleJoinRoom;
		Photon.OnLeftLobbyEvent += HandleLeavingLobby;
		Photon.OnPhotonCreateRoomFailedEvent += HandleCreateRoomFailed;
		Photon.OnPhotonJoinRoomFailedEvent += HandleJoinRoomFailed;
		Photon.OnPhotonRandomJoinFailedEvent += HandleRandomJoinRoomFailed;
		Photon.OnPhotonPlayerConnectedEvent += HandlePlayerJoinRoom;

		_Lobby.gameObject.SetActive(true);
		RobotController.IsCheering = false;
		Global.Camera.SetActiveCamera("Lobby");
		PhotonNetwork.GetCustomRoomList(TypedLobby.Default, "");
		BlackFader.Fade(false, 0.5f, () =>
		{
			_FindGameButton.onClick.AddListener(OnFindGameButton);
			_ExitLobbyButton.onClick.AddListener(OnExitLobbyButton);
			_bSearchingForRoom = false;
			_bWaitingForPlayer = false;
		});
	}

	public override void Update()
	{
		base.Update();
	}

	public override void LateUpdate()
	{
		base.LateUpdate();
	}

	public override void OnExit()
	{
		base.OnExit();

		_Lobby.gameObject.SetActive(false);
		Photon.OnCreatedRoomEvent -= HandleCreateRoom;
		Photon.OnJoinedRoomEvent -= HandleJoinRoom;
		Photon.OnLeftLobbyEvent -= HandleLeavingLobby;
		Photon.OnPhotonCreateRoomFailedEvent -= HandleCreateRoomFailed;
		Photon.OnPhotonJoinRoomFailedEvent -= HandleJoinRoomFailed;
		Photon.OnPhotonRandomJoinFailedEvent -= HandleRandomJoinRoomFailed;
		Photon.OnPhotonPlayerConnectedEvent -= HandlePlayerJoinRoom;

		_FindGameButton.onClick.RemoveListener(OnFindGameButton);
		_ExitLobbyButton.onClick.RemoveListener(OnExitLobbyButton);
	}

	private void OnFindGameButton()
	{
		if(!_bSearchingForRoom)
		{
			SetStatus("Searching for Room...");
			_bSearchingForRoom = true;
			Photon.JoinRoom();
		}
	}

	private void OnExitLobbyButton()
	{
		if(!Photon.LeaveLobby())
		{
			LeaveLobby();
		}
	}

	private void HandleJoinRoom()
	{
		SetStatus("Joined Room");
		if(Photon.CanStartGame() == false)
		{
			_bWaitingForPlayer = true;
			SetStatus("Waiting for Player...");
		}
		else
		{
			StartGame();
		}
	}

	private void HandleCreateRoom()
	{
		SetStatus("Room Created... Joining Room...");
	}

	private void HandleJoinRoomFailed(object[] codeAndMsg)
	{
		SetStatus("Failed to Join Room");
		_bSearchingForRoom = false;
	}

	private void HandleRandomJoinRoomFailed(object[] codeAndMsg)
	{
		if(Photon.CreateRoom())
		{
			SetStatus("Creating Room...");
		}
		else
		{
			SetStatus("Failed to create room");
		}
	}

	private void HandleCreateRoomFailed(object[] codeAndMsg)
	{
		SetStatus("Failed to Create Room");		
		_bSearchingForRoom = false;
	}

	private void HandleLeavingLobby()
	{
		// TODO: Check if leaving for disconnect or joining a room
		// LeaveLobby();
	}

	private void HandlePlayerJoinRoom(PhotonPlayer player)
	{
		if(_bWaitingForPlayer)
		{
			if(Photon.CanStartGame())
			{
				StartGame();
			}
		}
	}

	private void LeaveLobby()
	{
		FSM.ChangeState("Disconnect");
	}

	private void StartGame()
	{
		ResetGame();
		FSM.ChangeState("Draw");
	}
}
