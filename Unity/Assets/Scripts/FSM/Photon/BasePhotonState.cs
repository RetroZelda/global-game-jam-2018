﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Retro.FSM;
using Retro.Log;
using DG.Tweening;

public class BasePhotonState : IState
{
	public enum PlayerSide { Red, Blue };
	public delegate void PhotonEvent();

	public PhotonEvent OnPhotonFSMClose;

    private FSMChapter _FSM = null;
    private RetroLogger _Logger = null;
    private RectTransform _Canvas = null;

	private RectTransform _Status = null;
	private CanvasGroup _StatusGroup = null;
	private Text _StatusText = null;

    protected FSMChapter FSM { get { return _FSM ?? RetroFSM.Chapter("PhotonState"); } }
    protected RectTransform UI 
	{
		get 
		{
			if(!_Canvas)
			{
				Object[] canvases = GameObject.FindObjectsOfType<Canvas>();
				foreach(Object obj in canvases)
				{
					Canvas canvas = obj as Canvas;
					if(canvas.gameObject.tag == "MainCanvas")
					{
						_Canvas = canvas.transform.Find("Gameplay") as RectTransform;
						break;
					}
				}
			}
			return _Canvas;
		}
	}
	
    private RetroLogger Logger { get { return _Logger ?? RetroLog.RetrieveCatagory(StateInfo.Attribute.MachineID); } }
	protected PhotonManager Photon { get { return Global.Photon; } }


	protected static ScoreKeeper _ScoreManager { get; set; }
	protected static ScoreKeeper ScoreManager { get { return _ScoreManager ?? Transform.FindObjectOfType<ScoreKeeper>(); } }
	protected static int CurRound { get; set; }
	protected static int MyScore { get; set; }
	protected static int OpponentScore { get; set; }

	protected static void ResetGame()
	{
		CurRound = 0;
		MyScore = 0;
		OpponentScore = 0;
		ScoreManager.Reset();
	}

	public override void OnCreate(params object[] _InitObjects)
	{
		OnPhotonFSMClose = _InitObjects[1] as PhotonEvent;
		_Canvas = _InitObjects[2] as RectTransform;

		_Status = UI.root.Find("PhotonStatus") as RectTransform;
		_StatusGroup = _Status.GetComponent<CanvasGroup>();
		_StatusText = _Status.Find("Text").GetComponent<Text>();

		SetStatus("");
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
	}
	
	public override void PriorityUpdate()
	{
	}
	
	public override void Update()
	{

	}
	
	public override void LateUpdate()
	{

	}
	
	public override void OnExit()
	{
		
	}
	
	public override void OnDestroy()
	{
		OnPhotonFSMClose = null;
	}
	
	public override void OnPause()
	{

	}
	
	public override void OnResume()
	{

	}
		
	public void Log(string szLog)
	{
		Logger.Log(szLog);
	}

	protected void SetStatus(string szStatus)
	{
		bool bHasText = !string.IsNullOrEmpty(szStatus);

		if((_StatusText.text != "") != bHasText)
		{
			_StatusGroup.DOFade(bHasText == true ? 1.0f : 0.0f, 0.5f);
		}
		_StatusText.text = szStatus;

		if(bHasText)
		{
			Log("Status: " + szStatus);
		}
	}

	protected void CloseFSM()
	{
		// this FSM is run through script so we need to tell our creator that we are done
		if(OnPhotonFSMClose != null)
		{
			OnPhotonFSMClose.Invoke();
		}
	}
	
}
