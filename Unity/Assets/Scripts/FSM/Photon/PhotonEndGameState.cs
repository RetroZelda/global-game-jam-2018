﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Retro.FSM;
using Retro.Log;
using DG.Tweening;

[FSMState("PhotonState", "EndGame")]
public class PhotonEndGameState : BasePhotonState
{
	private ResultsCutscenePlayer _Cutscene;

	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);

		_Cutscene = GameObject.FindObjectOfType<ResultsCutscenePlayer>();
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);
		bool bWon = (bool)_PassthroughObjects[0];

		Transform trans = bWon == true ? _Cutscene.WinnerCamera : _Cutscene.LoserCamera;

		Global.Camera.SetActiveCamera("");
		Global.MainCamera.transform.position = trans.position - (trans.forward * 1.0f);
		Global.MainCamera.transform.rotation = trans.rotation;
		Global.MainCamera.transform.DOMove(trans.position, 9.0f);

		BlackFader.Fade(false, 0.5f, () => 
		{
			_Cutscene.OnCutsceneFinished += HandleCutsceneFinished;
			_Cutscene.StartCutscene(bWon);
		});
	}

	public override void OnExit()
	{
		base.OnExit();
		_Cutscene.OnCutsceneFinished -= HandleCutsceneFinished;
	}

	private void HandleCutsceneFinished()
	{
		BlackFader.Fade(true, 0.5f, () => 
		{
			FSM.ChangeState("Connect");
		});
	}
}
