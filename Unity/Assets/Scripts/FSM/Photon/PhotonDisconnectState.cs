﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Retro.FSM;
using Retro.Log;

[FSMState("PhotonState", "Disconnect")]
public class PhotonDisconnectState : BasePhotonState
{
	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);
	}

	public override void Update()
	{
		base.Update();
	}

	public override void LateUpdate()
	{
		base.LateUpdate();
	}

	public override void OnExit()
	{
		base.OnExit();
	}
}
