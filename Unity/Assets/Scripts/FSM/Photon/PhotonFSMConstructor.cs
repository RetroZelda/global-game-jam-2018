﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Retro.FSM;

public class PhotonFSMConstructor : BaseFSMConstructor<BasePhotonState>
{
	public override string[] MachineHierarchy { get { return new string[] {"PhotonState"}; } }
	public override void Init(params object[] initObjects)
    {
        // the Chapter is how we Identify this FSM
        Chapter = "PhotonState";
    }
}
