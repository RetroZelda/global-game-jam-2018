﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Retro.FSM;
using DG.Tweening;

[FSMState("PhotonState", "Connect", true)]
public class PhotonConnectState : BasePhotonState
{
	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);

		SetStatus("Connecting...");
		Photon.OnConnectedToMasterEvent += HandleMasterConnect;
		Photon.OnFailedToConnectToPhotonEvent += HandleConnectionFailed;
		Photon.OnConnectionFailEvent += HandleConnectionFailed;
		Photon.OnJoinedLobbyEvent += HandleJoiningLobby;

		if(Photon.Connect())
		{
			HandleMasterConnect();
		}
	}

	public override void Update()
	{
		base.Update();

		if(Input.GetKeyDown(KeyCode.Space))
		{
			CloseFSM();
		}
	}

	public override void LateUpdate()
	{
		base.LateUpdate();
	}

	public override void OnExit()
	{
		base.OnExit();

		Photon.OnConnectedToMasterEvent -= HandleMasterConnect;
		Photon.OnFailedToConnectToPhotonEvent -= HandleConnectionFailed;
		Photon.OnConnectionFailEvent -= HandleConnectionFailed;
		Photon.OnJoinedLobbyEvent -= HandleJoiningLobby;
	}

	private void HandleMasterConnect()
	{
		if(Photon.JoinLobby())
		{
			SetStatus("Joining Lobby...");
		}
		else
		{
			SetStatus("Failed to join lobby...");
		}
	}

	private void HandleJoiningLobby()
	{
		SetStatus("");
		FSM.ChangeState("Lobby");
	}

	private void HandleConnectionFailed(DisconnectCause cause)
	{
		string szReason = "Failed to connect:\n";

		switch(cause)
		{
			case DisconnectCause.DisconnectByServerUserLimit:
			szReason += "Possible cause: The server's user limit was hit and client was forced to disconnect (on connect).";
			break;
			case DisconnectCause.ExceptionOnConnect:
				szReason += "Possible cause: Local server not running.";
			break;
			case DisconnectCause.DisconnectByServerTimeout:
				szReason += "Timeout disconnect by server (which decided an ACK was missing for too long).";
			break;
			case DisconnectCause.DisconnectByServerLogic:
				szReason += "Possible cause: Server's send buffer full (too much data for client).";
			break;
			case DisconnectCause.Exception:
				szReason += "Some exception caused the connection to close.";
			break;
			case DisconnectCause.InvalidAuthentication:
				szReason += "(32767) The Photon Cloud rejected the sent AppId. Check your Dashboard and make sure the AppId you use is complete and correct.";
			break;
			case DisconnectCause.MaxCcuReached:
				szReason += "(32757) Authorization on the Photon Cloud failed because the concurrent users (CCU) limit of the app's subscription is reached.";
			break;
			case DisconnectCause.InvalidRegion:
				szReason += "(32756) Authorization on the Photon Cloud failed because the app's subscription does not allow to use a particular region's server.";
			break;
			case DisconnectCause.SecurityExceptionOnConnect:
				szReason += "The security settings for client or server don't allow a connection.\nhttp://doc.exitgames.com/photon-server/PolicyApp";
			break;
			case DisconnectCause.DisconnectByClientTimeout:
				szReason += "Timeout disconnect by client (which decided an ACK was missing for too long)";
			break;
			case DisconnectCause.InternalReceiveException:
				szReason += "Possible cause: Socket failure.";
			break;
			case DisconnectCause.AuthenticationTicketExpired:
				szReason += "(32753) The Authentication ticket expired. Handle this by connecting again (which includes an authenticate to get a fresh ticket).";
			break;
		}

		SetStatus(szReason);
		CloseFSM();
	}
}
