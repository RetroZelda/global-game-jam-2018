﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Retro.FSM;
using Retro.Log;
using DG.Tweening;

[FSMState("PhotonState", "Draw")]
public class PhotonDrawState : BasePhotonState
{
	private RectTransform _Drawing;
	private RectTransform _OpponentIsDone;
	private RectTransform _WaitingForOpponent;
	private RectTransform _Instructions;
	private Text _Round;
	private Text _Time;
	// private Button _FinishedButton;
	// private Button _UndoButton;

	private PlayerSide _MyTeam;
	private Transform _StartPosition;

	private DrawingPlayer _DrawingPlayer;
	private TravelingPlayer _TravelingPlayer;

	private Vector3[] _FinalPath;
	private Vector3[] _OpponentsPath;

	private float _fTime;

	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);

		_Drawing = UI.Find("Drawing") as RectTransform;
		_OpponentIsDone = _Drawing.Find("OpponentDone") as RectTransform;
		_WaitingForOpponent = _Drawing.Find("WaitingForOpponent") as RectTransform;
		_Instructions = _Drawing.Find("Instructions") as RectTransform;
		_Round = _Drawing.Find("Round").GetComponent<Text>();
		_Time = _Drawing.Find("Time").GetComponent<Text>();
		// _FinishedButton = _Drawing.Find("FinishedButton").GetComponent<Button>();
		// _UndoButton = _Drawing.Find("UndoButton").GetComponent<Button>();

		_Drawing.gameObject.SetActive(false);
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);

		SetStatus("");

		_MyTeam = Photon.IsRoomOwner() == true ? PlayerSide.Blue : PlayerSide.Red;
		_StartPosition = Global.GetStartPosition(_MyTeam);

		_DrawingPlayer = Global.Prefab.ObtainNewInstance("DrawingPlayer").GetComponent<DrawingPlayer>();
		_TravelingPlayer = Global.Prefab.ObtainNewInstance("TravelingPlayer").GetComponent<TravelingPlayer>();

		_DrawingPlayer.SetPlayerSide(_MyTeam);
		_TravelingPlayer.SetPlayerSide(_MyTeam);

		_DrawingPlayer.Activate();
		_TravelingPlayer.Deactivate();

		// Physics.IgnoreCollision(_DrawingPlayer.GetComponent<Collider>(), _TravelingPlayer.GetComponent<Collider>(), true);
		_DrawingPlayer.transform.position = _StartPosition.position;
		_TravelingPlayer.transform.position = _StartPosition.position;
		_DrawingPlayer.transform.rotation = _StartPosition.rotation;
		_TravelingPlayer.transform.rotation = _StartPosition.rotation;

		_FinalPath = null;
		_OpponentsPath = null;

		Global.Camera.SetActiveCamera("");
		Camera drawCamera = Global.Camera.Obtain("Drawing");
		Global.MainCamera.transform.DOMove(drawCamera.transform.position, 0.5f);
		Global.MainCamera.transform.DORotate(drawCamera.transform.rotation.eulerAngles, 0.5f).OnComplete(() => 
		{
        	Global.Camera.SetActiveCamera("Drawing");
		});

		_Drawing.gameObject.SetActive(true);
		_OpponentIsDone.gameObject.SetActive(false);
		_WaitingForOpponent.gameObject.SetActive(false);
		_Instructions.gameObject.SetActive(true);

		_DrawingPlayer.OnPathDrawn += OnPathFinished;
		Photon.OnPhotonDataEvent += OnDataEvent;

		_fTime = 30.0f; // Global.DrawTime;
		_Time.color = Color.red;
		_Round.text = string.Format("Round {0} / {1}", CurRound + 1, Global.NumberOfRounds);
		_DrawingPlayer.OnPointAdded += HandlePointAdded;

		RobotController.IsCheering = true;

		// _FinishedButton.onClick.AddListener(OnFinishButton);
		// _UndoButton.onClick.AddListener(OnUndoButton);
	}

	private void HandlePointAdded(Vector3 point)
	{
		if(_fTime > Global.DrawTime)
		{
			_fTime = Global.DrawTime;
		}
		_Time.color = Color.green;
		_DrawingPlayer.OnPointAdded -= HandlePointAdded;
		_Instructions.gameObject.SetActive(false);
	}

	public override void Update()
	{
		base.Update();

		if(_fTime > 0.0f)
		{
			_fTime -= Time.deltaTime;
			if(_fTime < 0.0f)
			{
				_fTime = 0.0f;
				OnFinishButton();
			}
			SetTime(_fTime.ToString("00.0s"));
		}
	}

	public override void LateUpdate()
	{
		base.LateUpdate();
	}

	public override void OnExit()
	{
		base.OnExit();

		// _FinishedButton.onClick.RemoveListener(OnFinishButton);
		// _UndoButton.onClick.RemoveListener(OnUndoButton);

		_Drawing.gameObject.SetActive(false);
		Photon.OnPhotonDataEvent -= OnDataEvent;
		_DrawingPlayer.OnPathDrawn -= OnPathFinished;
		_DrawingPlayer.OnPointAdded -= HandlePointAdded;

		GameObject.Destroy(_DrawingPlayer.gameObject);
		_DrawingPlayer = null;
	}

	private void OnFinishButton()
	{
		_DrawingPlayer.FinishDrawing();
		_DrawingPlayer.Deactivate();
		// _FinishedButton.gameObject.SetActive(false);
		// _UndoButton.gameObject.SetActive(false);
	}

	private void OnUndoButton()
	{
		
	}

	private void OnDataEvent(byte eventcode, object content, int senderid)
	{
		if(eventcode == 1)
		{
			_OpponentsPath = content as Vector3[];
			if(_FinalPath == null)
			{
				_OpponentIsDone.gameObject.SetActive(true);
			}
			else
			{
				BeginBattle();
			}
		}
	}

	private void OnPathFinished(Vector3[] path)
	{
		_FinalPath = path;
		Photon.RaiseDataEvent(1, _FinalPath, true);

		if(_OpponentsPath == null)
		{
			_WaitingForOpponent.gameObject.SetActive(true);
		}
		else
		{
			BeginBattle();
		}
	}

	private void BeginBattle()
	{
		FSM.ChangeState("Battle", _FinalPath, _OpponentsPath, _TravelingPlayer, _MyTeam);
	}

	private void SetTime(string szText)
	{
		_Time.text = szText;
	}
}
