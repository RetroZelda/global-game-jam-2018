﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Retro.FSM;
using DG.Tweening;

[FSMState("PhotonState", "Battle")]
public class PhotonBattleState : BasePhotonState
{
	private Vector3[] _MyPath;
	private Vector3[] _OpponentPath;

	private TravelingPlayer _MyPlayer;
	private TravelingPlayer _OpponentPlayer;

	private PlayerSide _MyTeam;
	private PlayerSide _OpponentTeam;

	private int _nPathFinishedCount;

	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);

		_MyPath = _PassthroughObjects[0] as Vector3[];
		_OpponentPath = _PassthroughObjects[1] as Vector3[];
		_MyPlayer = _PassthroughObjects[2] as TravelingPlayer;
		_MyTeam = (PlayerSide)_PassthroughObjects[3];

		_OpponentTeam = _MyTeam == PlayerSide.Blue ? PlayerSide.Red : PlayerSide.Blue;
		_OpponentPlayer = Global.Prefab.ObtainNewInstance("TravelingPlayer").GetComponent<TravelingPlayer>();

		_OpponentPlayer.SetPlayerSide(_OpponentTeam);

		_MyPlayer.Activate();
		_OpponentPlayer.Activate();
		
		Transform myStart = Global.GetStartPosition(_MyTeam);
		Transform opponentStart = Global.GetStartPosition(_OpponentTeam);

		_MyPlayer.transform.position = myStart.position;
		_MyPlayer.transform.rotation = myStart.rotation;
		_OpponentPlayer.transform.position = opponentStart.position;
		_OpponentPlayer.transform.rotation = opponentStart.rotation;

		_MyPlayer.SetPath(_MyPath);
		_OpponentPlayer.SetPath(_OpponentPath);

/*
		GameObject lineGO0 = new GameObject("0", typeof(LineRenderer));
		LineRenderer line0 = lineGO0.GetComponent<LineRenderer>();
		line0.startWidth = line0.endWidth = 0.05f;
		line0.startColor = line0.endColor = Color.blue;
		line0.positionCount = _MyPath.Length;
		line0.SetPositions(_MyPath);

		GameObject lineGO1 = new GameObject("1", typeof(LineRenderer));
		LineRenderer line1 = lineGO1.GetComponent<LineRenderer>();
		line1.startWidth = line1.endWidth = 0.05f;
		line1.startColor = line1.endColor = Color.red;
		line1.positionCount = _OpponentPath.Length;
		line1.SetPositions(_OpponentPath);
*/

		Camera battleCamera = Global.Camera.Obtain("Battle");
		Global.Camera.SetActiveCamera("");
		Global.MainCamera.transform.DOMove(battleCamera.transform.position, 0.5f);
		Global.MainCamera.transform.DORotate(battleCamera.transform.rotation.eulerAngles, 0.5f).OnComplete(() => 
		{
			_MyPlayer.StartTraveling();
			_OpponentPlayer.StartTraveling();
        	Global.Camera.SetActiveCamera("Battle");
		});	

		_nPathFinishedCount = 0;
		Global.Photon.OnLeftRoomEvent += HandleLeaveRoom;
		Global.KillPlane.OnPlayerDeath += HandlePlayerDeathEvent;
		_MyPlayer.OnPathEnd += HandlePathEnd;
		_OpponentPlayer.OnPathEnd += HandlePathEnd;
	}

	private void HandlePathEnd(Transform trans)
	{
		if(++_nPathFinishedCount >= 2)
		{
			HandlePlayerDeathEvent(null); // null = no one died
		}
	}

	public override void Update()
	{
		base.Update();
	}

	public override void LateUpdate()
	{
		base.LateUpdate();
	}

	public override void OnExit()
	{
		base.OnExit();

		Global.Photon.OnLeftRoomEvent -= HandleLeaveRoom;
		Global.KillPlane.OnPlayerDeath -= HandlePlayerDeathEvent;

		GameObject.Destroy(_MyPlayer.gameObject);
		GameObject.Destroy(_OpponentPlayer.gameObject);
	}

	private void HandlePlayerDeathEvent(BasePlayer player)
	{
		if(player == _MyPlayer)
		{
			// we win
			MyScore++;
			ScoreManager.SetScoreIndex(CurRound, false); // backwards because idk why
		}
		else if(player == _OpponentPlayer)
		{
			// they win
			OpponentScore++;
			ScoreManager.SetScoreIndex(CurRound, true); // backwards because idk why
		}

		_MyPlayer.StopTraveling();
		_OpponentPlayer.StopTraveling();

		Global.KillPlane.OnPlayerDeath -= HandlePlayerDeathEvent;

		Global.Instance.StartCoroutine(WaitSomeTime(2.0f));
	}

	private IEnumerator WaitSomeTime(float fTime)
	{
		yield return new WaitForSeconds(fTime);
		if(++CurRound == Global.NumberOfRounds)
		{
			EndGame();
		}
		else
		{
			ContinueGame();
		}
	}

	private void EndGame()
	{
		Photon.LeaveRoom();
		// EndGame
	}

	private void ContinueGame()
	{
		FSM.ChangeState("Draw");
	}

	private void HandleLeaveRoom()
	{
		// go to end screen because we are done

		BlackFader.Fade(true, 0.5f, () => 
		{
			                          // backwards because idk why
			FSM.ChangeState("EndGame", MyScore < OpponentScore); // true = we win
		});
	}
}
