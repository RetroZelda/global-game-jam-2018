﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Retro.FSM;
using DG.Tweening;

[FSMState("GameplayState", "Gameplay")]
public class GameplayState : BaseGameState
{
	private RectTransform _GamePlay;
	protected FSMChapter _PhotonFSM;

	private RectTransform _Message = null;
	private CanvasGroup _MEssageGroup = null;
	private Text _MessageText = null;
	private Button _MessageButton = null;

	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);

		_GamePlay = UI.Find("Gameplay") as RectTransform;

		_GamePlay.gameObject.SetActive(false);
		_PhotonFSM = null;
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);
		
		Log("Creating Photon FSM");
		BasePhotonState.PhotonEvent onDoneEvent = OnPhotonDone;


		_GamePlay.gameObject.SetActive(true);
		RetroFSM.StartNewChapter<PhotonFSMConstructor>(onDoneEvent, _GamePlay);
		_PhotonFSM = RetroFSM.Chapter("PhotonState");

		Global.Photon.OnPhotonPlayerDisconnectedEvent += HandePlayerDisconnected;
		Global.Photon.OnDisconnectedFromPhotonEvent += HandleDisconnectedFromPhoton;
		Global.Photon.OnPhotonPlayerActivityChangedEvent += HandlePhotonPlayerActivityChanged;
	}

	public override void Update()
	{
		base.Update();
	}

	public override void LateUpdate()
	{
		base.LateUpdate();
	}

	public override void OnExit()
	{
		base.OnExit();

		Global.Photon.OnPhotonPlayerDisconnectedEvent -= HandePlayerDisconnected;
		Global.Photon.OnDisconnectedFromPhotonEvent -= HandleDisconnectedFromPhoton;
		Global.Photon.OnPhotonPlayerActivityChangedEvent -= HandlePhotonPlayerActivityChanged;
	}

	private void OnPhotonDone()
	{
		Log("Destroying Photon FSM");

		if(_PhotonFSM != null)
		{
			_PhotonFSM.StopChapter();
			_PhotonFSM = null;
		}
		else
		{
			RetroFSM.StopChapter<PhotonFSMConstructor>();
		}

		BlackFader.Fade(true, 0.5f, () =>
		{
			_GamePlay.gameObject.SetActive(false);
			FSM.ChangeState("MainMenu");
		});
	}

	private void HandePlayerDisconnected(PhotonPlayer otherPlayer)
	{
		Log("Player disconnected.");
		Global.Photon.LeaveRoom();
	}

	private void HandleDisconnectedFromPhoton()
	{
		Log("Disconnected from Photon.");
		OnPhotonDone();
	}

	private void HandlePhotonPlayerActivityChanged(PhotonPlayer otherPlayer)
	{
		if(otherPlayer.IsInactive)
		{
			Log("Other Player is Inactive; Leaving Room");
			Global.Photon.LeaveRoom();
		}
	}
}
