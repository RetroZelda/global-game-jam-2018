﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Retro.FSM;
using Retro.Log;

public class BaseGameState : IState
{
    private FSMChapter _FSM = null;
    private RetroLogger _Logger = null;
    private RectTransform _Canvas = null;

    protected FSMChapter FSM { get { return _FSM ?? RetroFSM.Chapter("GameplayState"); } }
    protected RectTransform UI 
	{
		get 
		{
			if(!_Canvas)
			{
				Object[] canvases = GameObject.FindObjectsOfType<Canvas>();
				foreach(Object obj in canvases)
				{
					Canvas canvas = obj as Canvas;
					if(canvas.gameObject.tag == "MainCanvas")
					{
						_Canvas = canvas.GetComponent<RectTransform>();
						break;
					}
				}
			}
			return _Canvas;
		}
	}
	
    private RetroLogger Logger { get { return _Logger ?? RetroLog.RetrieveCatagory(StateInfo.Attribute.MachineID); } }

	public bool IsTouchingScreen { get; private set; }
	public bool WasTouchingScreen { get; private set; }

	public override void OnCreate(params object[] _InitObjects)
	{

	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		IsTouchingScreen = false;
		WasTouchingScreen = false;
	}
	
	public override void PriorityUpdate()
	{
		WasTouchingScreen = IsTouchingScreen;
		IsTouchingScreen = Input.touchCount > 0 || Input.GetMouseButton(0);
	}
	
	public override void Update()
	{

	}
	
	public override void LateUpdate()
	{

	}
	
	public override void OnExit()
	{

	}
	
	public override void OnDestroy()
	{

	}
	
	public override void OnPause()
	{

	}
	
	public override void OnResume()
	{

	}
		
	public void Log(string szLog)
	{
		Logger.Log(szLog);
	}
}
