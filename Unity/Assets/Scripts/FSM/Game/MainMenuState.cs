﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Retro.FSM;
using DG.Tweening;

[FSMState("GameplayState", "MainMenu")]
public class MainMenuState : BaseGameState
{
	RectTransform _MainMenu;
	Button _PlayButton;
	// Button _SettingsButton;
	Button _ExitButton;

	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);

		_MainMenu = UI.Find("MainMenu") as RectTransform;
		_PlayButton = _MainMenu.Find("Buttons/PlayButton").GetComponent<Button>();
		// _SettingsButton = _MainMenu.Find("Buttons/SettingsButton").GetComponent<Button>();
		_ExitButton = _MainMenu.Find("Buttons/ExitButton").GetComponent<Button>();

		_MainMenu.gameObject.SetActive(false);
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);

		_MainMenu.gameObject.SetActive(true);
		BlackFader.Fade(false, 0.5f, () => 
		{
			_PlayButton.onClick.AddListener(OnPlayButton);
			// _SettingsButton.onClick.AddListener(OnSettingsButton);
			_ExitButton.onClick.AddListener(OnExitButton);
		});		

        Global.Camera.SetActiveCamera("MainMenu");
	}

	public override void OnExit()
	{
		base.OnExit();

		_MainMenu.gameObject.SetActive(false);
		_PlayButton.onClick.RemoveListener(OnPlayButton);
		// _SettingsButton.onClick.RemoveListener(OnSettingsButton);
		_ExitButton.onClick.RemoveListener(OnExitButton);
	}
	
	private void OnPlayButton()
	{
		BlackFader.Fade(true, 0.5f, () => 
		{
			FSM.ChangeState("Gameplay");
		});		
	}
	
	private void OnSettingsButton()
	{
		
	}
	
	private void OnExitButton()
	{
		BlackFader.Fade(true, 0.5f, () => 
		{
			#if UNITY_EDITOR
         		UnityEditor.EditorApplication.isPlaying = false;
			#else
				Application.Quit();
			#endif
		});
	}

}
