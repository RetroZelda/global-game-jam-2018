﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Retro.FSM;
using DG.Tweening;
using Lean.Touch;

[FSMState("GameplayState", "Title")]
public class TitleState : BaseGameState
{
	private RectTransform _Title;
	public override void OnCreate(params object[] _InitObjects)
	{
		base.OnCreate(_InitObjects);

		_Title = UI.Find("Title") as RectTransform;

		_Title.gameObject.SetActive(false);
	}

	public override void OnEnter(params object[] _PassthroughObjects)
	{
		base.OnEnter(_PassthroughObjects);

		BlackFader.Fade(false, 0.5f, () =>
		{
			_Title.gameObject.SetActive(true);
			LeanTouch.OnFingerTap += OnScreenTap;
		});
	}

	public override void OnExit()
	{
		base.OnExit();

		_Title.gameObject.SetActive(false);
	}

	private void OnScreenTap(LeanFinger finger)
	{
		LeanTouch.OnFingerTap -= OnScreenTap;
		BlackFader.Fade(true, 0.5f, () =>
		{
			FSM.ChangeState("MainMenu");
		});
	}
	
}
