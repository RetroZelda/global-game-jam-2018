﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Retro.FSM;

[FSMState("GameplayState", "SplashScreen", true)]
public class SplashScreenState : BaseGameState
{
	
    private Transform _SplashScreen;
    private Transform _BlackBG;
    private Image[] _SplashImages;

    private GameObject _RunnerPrefab;
    private SplashScreenRunner _Runner;

    int nCurSplashImage = 0;
    private bool bIsLeaving = false;
    public override void OnCreate(params object[] _InitObjects)
    {
        _SplashScreen = UI.Find("SplashScreen");
        _BlackBG = _SplashScreen.Find("Black");
        _RunnerPrefab = Global.Instance.PrefabLibrary.Obtain("splash_3dObjects");

        // ignore the black BG
        _BlackBG.SetParent(null);
        _SplashImages = _SplashScreen.GetComponentsInChildren<Image>();
        _BlackBG.SetParent(_SplashScreen);
        _BlackBG.SetAsFirstSibling();
    }

    public override void OnEnter(params object[] _PassthroughObjects)
    {
        // start black
        _SplashScreen.gameObject.SetActive(true);
        _BlackBG.gameObject.SetActive(true);
        BlackFader.ForceFade(false);
        nCurSplashImage = -1;
        bIsLeaving = false;

        // another hacky thing to prevent a flash of a random camera
        GameObject hackBlackCam = GameObject.Find("BLACKCAMERA");
        GameObject.Destroy(hackBlackCam);

        // hacky way to start them all invisible
        foreach(Image splash in _SplashImages)
        {
            splash.DOFade(0.0f, 0.0f);
        }
        FadeNextImage();

        Global.Camera.SetActiveCamera("MainMenu");
    }

    public override void Update()
    {
    }

    public override void LateUpdate()
    {
        base.LateUpdate();

        if(!WasTouchingScreen && IsTouchingScreen && !bIsLeaving)
        {
            bIsLeaving = true;
            foreach(Image splash in _SplashImages)
            {
                splash.StopAllCoroutines();
            }

            if(_Runner != null)
            {
                _Runner.StopAllCoroutines();   
            }

            BlackFader.Fade(true, 0.5f, () => 
            {
                if(_Runner != null)
                {
                    GameObject.Destroy(_Runner.gameObject);
                    _Runner = null;
                }

                LeaveState();
            });
        }
    }

    private void LeaveState()
    {
        FSM.ChangeState("Title");
    }

    public override void OnExit()
    {
        _SplashScreen.gameObject.SetActive(false);
    }

    private void FadeNextImage()
    {
        nCurSplashImage++;
        if(nCurSplashImage < _SplashImages.Length)
        {
            if (_SplashImages[nCurSplashImage].name == "FullFrontal")
            {
                _SplashImages[nCurSplashImage].StartCoroutine(HandleFullFrontal(_SplashImages[nCurSplashImage]));
            }
            else
            {
                _SplashImages[nCurSplashImage].StartCoroutine(HandleSplash(_SplashImages[nCurSplashImage]));
            }
        }
        else
        {
            if(!bIsLeaving)
            {
                bIsLeaving = true;
                BlackFader.Fade(true, 0.5f, () => 
                {
                    LeaveState();
                });
            }
        }
    }

    private IEnumerator HandleFullFrontal(Image splash)
    {
        float fWaitTime = 1.0f;
        float fFadeTime = 3.0f;
        bool bRunnerFinished = false;

        // spawn the runner
        GameObject runnerObj = GameObject.Instantiate<GameObject>(_RunnerPrefab);
        _Runner = runnerObj.GetComponent<SplashScreenRunner>();
        _Runner.Reset();

        splash.DOFade(0.0f, 0.0f);
        splash.DOFade(1.0f, fFadeTime);
        yield return new WaitForSeconds(fFadeTime);

        // wait for the runner to finish
        _Runner.OnRunnerComplete = () => { bRunnerFinished = true; };
        //_BlackBG.gameObject.SetActive(false);
        _BlackBG.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        _Runner.StartRunner();

        while (bRunnerFinished == false)
        {
            yield return null;
        }

        // _BlackBG.gameObject.SetActive(true);
        _BlackBG.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 1.0f);
        _Runner.OnRunnerComplete = null;
        GameObject.Destroy(runnerObj);
        _Runner = null;

        splash.DOFade(0.0f, fFadeTime).OnComplete(FadeNextImage);
        yield return new WaitForSeconds(fFadeTime + fWaitTime);
    }

    private IEnumerator HandleSplash(Image splash)
    {
        float fWaitTime = 1.0f;
        float fFadeTime = 3.0f;

        splash.DOFade(0.0f, 0.0f);
        splash.DOFade(1.0f, fFadeTime);
        yield return new WaitForSeconds(fFadeTime + fWaitTime);
        splash.DOFade(0.0f, fFadeTime).OnComplete(FadeNextImage);
        yield return new WaitForSeconds(fFadeTime + fWaitTime);
    }
}
