﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Retro.FSM;

public class GameFSMConstructor : BaseFSMConstructor<BaseGameState>
{
    // the MachineHierarchy handles FSM heirarchys.
    // This allows you to have child FSM states on diifferent FSMs, but shares the state logic of its parent
    public override string[] MachineHierarchy { get { return new string[] {"GameplayState"}; } }
    
    // This would be an example of having an overide FSM
    // The order is important.  MachineHierarchy[0] is always the root; MachineHierarchy[size-1] is the leaf state
    // public override string[] MachineHierarchy { get { return new string[] {"ExampleFSM", "ExampleFSMOverride"}; } }



    public override void Init(params object[] initObjects)
    {
        // the Chapter is how we Identify this FSM
        Chapter = "GameplayState";
    }
}