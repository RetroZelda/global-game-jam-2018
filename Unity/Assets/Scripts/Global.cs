﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioManager), typeof(PhotonManager))]
public class Global : MonoBehaviour 
{
	public static Global Instance { get; private set; }

	[Header("Game")]

	[SerializeField]
	[Range(1, 10)]
	private int _NumRounds = 3;

	[SerializeField]
	private float _DrawTime = 10.0f;

	[SerializeField]
	private float _FixedStep = 0.016f;

	[SerializeField]
	private Camera _MainCamera;

	[SerializeField]
	private KillPlane _KillVolume;
	
	[SerializeField]
	private Transform _RedPlayerStartPosition;

	[SerializeField]
	private Transform _BluePlayerStartPosition;

	[Header("Libraries")]
	[SerializeField]
	private PrefabLibrary _PrefabLibrary;

	[SerializeField]
	private AudioLibrary _AudioLibrary;

	public int NumRounds { get { return _NumRounds; } }
	public float DrawTimer { get { return _DrawTime; } }
	public float FixedTimeStep { get { return _FixedStep; } }
	public Camera MainGameCamera { get { return _MainCamera; } }
	public KillPlane KillVolumePlane { get { return _KillVolume; } }

	public Transform RedPlayerStartPosition { get { return _RedPlayerStartPosition; } }
	public Transform BluePlayerStartPosition { get { return _BluePlayerStartPosition; } }

	public PrefabLibrary PrefabLibrary { get { return _PrefabLibrary; } }
	public AudioLibrary AudioLibrary { get { return _AudioLibrary; } }

	private AudioManager _Audio;
	public AudioManager AudioMan { get { return _Audio ?? GetComponent<AudioManager>(); } }

	private PhotonManager _Photon;
	public PhotonManager PhotonMan { get { return _Photon ?? GetComponent<PhotonManager>(); } }

	private CameraLibrary _CameraLib;
	public CameraLibrary CameraLib { get { return _CameraLib ?? FindObjectOfType<CameraLibrary>(); } }

	public static int NumberOfRounds { get { return Instance.NumRounds; } }
	public static float FixedStep { get { return Instance.FixedTimeStep; } }
	public static float DrawTime { get { return Instance.DrawTimer; } }
	public static Camera MainCamera { get { return Instance.MainGameCamera; } }
	public static KillPlane KillPlane { get { return Instance.KillVolumePlane; } }
	public static CameraLibrary Camera { get { return Instance.CameraLib; } }
	public static PrefabLibrary Prefab { get { return Instance.PrefabLibrary; } }
	public static AudioManager Audio { get { return Instance.AudioMan; } }
	public static PhotonManager Photon { get { return Instance.PhotonMan; } }
	public static Transform RedStartPosition { get { return Instance.RedPlayerStartPosition; } }
	public static Transform BlueStartPosition { get { return Instance.BluePlayerStartPosition; } }

	public static Transform GetStartPosition(BasePhotonState.PlayerSide playerTeam)
	{
		if(playerTeam == BasePhotonState.PlayerSide.Blue)
		{
			return BlueStartPosition;
		}
		else if(playerTeam == BasePhotonState.PlayerSide.Red)
		{
			return RedStartPosition;
		}
		return null;
	}

	void Awake () 
	{
		if(Instance != null)
		{
			Debug.LogError("Global already defined!");
		}
		Instance = this;

		SceneManager.LoadScene(1, LoadSceneMode.Additive);
	}
}
